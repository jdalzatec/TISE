import gi
from gi.repository import Gtk, Pango
gi.require_version('Gtk', '3.0')


class Window(Gtk.Window):
    def __init__(self, title):
        Gtk.Window.__init__(self, title=title)
        self.modify_font(Pango.FontDescription('Ubuntu Bold'))


class VBox(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=2)

    def append(self, widget, expand=False):
        self.pack_start(widget, expand, expand, 10)


class HBox(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL,
                         spacing=2)

    def append(self, widget, expand=False):
        self.pack_start(widget, expand, expand, 10)


class Label(Gtk.Label):
    def __init__(self, label):
        Gtk.Label.__init__(self, label=label)
        self.set_use_markup(True)
        self.set_alignment(0, 0.5)


class Image(Gtk.Image):
    def __init__(self, path=None):
        Gtk.Image.__init__(self, file=path)


class Button(Gtk.Button):
    def __init__(self, label):
        Gtk.Button.__init__(self, label=label)


class Entry(Gtk.Entry):
    def __init__(self, hint=None):
        Gtk.Entry.__init__(self, placeholder_text=hint)
