Compile with python3 the file TISE.py.

The reserved variables to type in the textbox for the basis set and potential are:
width -> width of the infinite potential well
n     -> principal quantum number
x     -> array of values of x

All numpy functions defined in
http://docs.scipy.org/doc/numpy/reference/routines.math.html can be used without
the initial word "numpy".

All comments and suggestions, please send to jdalzatec@unal.edu.co