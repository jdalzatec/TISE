from interface import *
from matplotlib.backends.backend_gtk3agg \
    import FigureCanvasGTK3Agg as FigureCanvas
from matplotlib.backends.backend_gtk3 \
    import NavigationToolbar2GTK3 as NavigationToolbar
from matplotlib import pyplot
from matplotlib.patches import Rectangle
import numpy
from numpy import *
from scipy.integrate import simps
from calculations import *






ERROR = 1e-6
E_ordered = None
C_ordered = None
C_prime = None

btn_n_PAGE2 = None
btn_K_PAGE2 = None
def main():
    window = Window("TISE")

    notebook = Gtk.Notebook()

    parentUniquePotential__PAGE1 = VBox()
    parentPeriodicPotential_PAGE2 = VBox()




    hbox0_PAGE1 = HBox()
    hbox1_PAGE1 = HBox()
    hbox2_PAGE1 = HBox()
    hbox3_PAGE1 = HBox()
    hbox7_PAGE1 = HBox()
    vbox0_PAGE1 = VBox()
    vbox1_PAGE1 = VBox()

    parentUniquePotential__PAGE1.append(hbox0_PAGE1)
    parentUniquePotential__PAGE1.append(hbox1_PAGE1)
    parentUniquePotential__PAGE1.append(hbox2_PAGE1)
    parentUniquePotential__PAGE1.append(hbox3_PAGE1)
    parentUniquePotential__PAGE1.append(hbox7_PAGE1, True)

    hbox7_PAGE1.append(vbox0_PAGE1)
    hbox7_PAGE1.append(vbox1_PAGE1, True)

    lblNormalized_PAGE1 = Label("Basis set is normalized ?")
    imgNormalized_PAGE1 = Image()
    hbox4_PAGE1 = HBox()
    hbox4_PAGE1.append(lblNormalized_PAGE1)
    hbox4_PAGE1.append(imgNormalized_PAGE1)

    lblCanNormalized_PAGE1 = Label("Basis set can be normalized ?")
    imgCanNormalized_PAGE1 = Image()
    hbox5_PAGE1 = HBox()
    hbox5_PAGE1.append(lblCanNormalized_PAGE1)
    hbox5_PAGE1.append(imgCanNormalized_PAGE1)

    lblOrthonormal_PAGE1 = Label("Basis set is orthonormal ?")
    imgOrthonormal_PAGE1 = Image()
    hbox6_PAGE1 = HBox()
    hbox6_PAGE1.append(lblOrthonormal_PAGE1)
    hbox6_PAGE1.append(imgOrthonormal_PAGE1)

    btnProbabilityFunction_PAGE1 = Button("Probability density function")
    hbox8_PAGE1 = HBox()
    hbox8_PAGE1.append(btnProbabilityFunction_PAGE1, True)

    btnWaveFunction_PAGE1 = Button("Wavefunction")
    hbox9_PAGE1 = HBox()
    hbox9_PAGE1.append(btnWaveFunction_PAGE1, True)

    vbox0_PAGE1.append(hbox4_PAGE1)
    vbox0_PAGE1.append(hbox5_PAGE1)
    vbox0_PAGE1.append(hbox6_PAGE1)
    vbox0_PAGE1.append(hbox8_PAGE1)
    vbox0_PAGE1.append(hbox9_PAGE1)

    lblWidth_PAGE1 = Label("Width      ")
    hbox0_PAGE1.append(lblWidth_PAGE1)

    txtWidth = Entry("With of the box")
    txtWidth.set_text("1.0")
    hbox0_PAGE1.append(txtWidth)

    lblDivsions_PAGE1 = Label("Divisions")
    hbox0_PAGE1.append(lblDivsions_PAGE1)

    txtDivisions = Entry("The amount of intervals in the space")
    txtDivisions.set_text("5000")
    hbox0_PAGE1.append(txtDivisions)

    lblAmountBasis_PAGE1 = Label("Amount of basis")
    hbox0_PAGE1.append(lblAmountBasis_PAGE1)

    txtAmountBasis = Entry("Insert amount of the basis")
    txtAmountBasis.set_text("20")
    hbox0_PAGE1.append(txtAmountBasis)

    lblBasis_PAGE1 = Label("Basis set ")
    txtBasis = Entry("Set of basis")
    chkDefaultBasis = Gtk.CheckButton("Default")
    chkDefaultBasis.connect("toggled", lambda chk: txtBasis.set_text(
        "sqrt(2 / width) * sin(n * pi * x / width)")
        if chk.get_active()
        else txtBasis.set_text(""))
    btnPreviewBasis_PAGE1 = Button("Preview")

    def preview_basis(widget, txtWidth, txtDivisions,
                      txtAmountBasis, txtBasis):
        try:
            width = float(txtWidth.get_text())
            ndiv = int(txtDivisions.get_text())
            N = int(txtAmountBasis.get_text())
            basis = txtBasis.get_text()

            vbox_PAGE1 = VBox()
            hbox_PAGE1 = HBox()
            vbox_PAGE1.append(hbox_PAGE1)
            winPreviewBasis = Window("Preview of basis set")
            winPreviewBasis.add(vbox_PAGE1)

            lbl_n_PAGE1 = Label("n = ")
            lbl_n_PAGE1.set_tooltip_text("Choose the number of basis 'n'")
            hbox_PAGE1.append(lbl_n_PAGE1)
            btn_n_PAGE1 = Gtk.SpinButton.new_with_range(1, N, 1)
            hbox_PAGE1.append(btn_n_PAGE1, True)

            n = int(btn_n_PAGE1.get_value())

            fig = pyplot.figure()
            ax = fig.add_subplot(111)
            x = numpy.linspace(0, width, ndiv)
            yreal = eval(basis).real
            yimag = eval(basis).imag
            ax.plot(x, yreal, lw=3, label="real")
            ax.plot(x, yimag, lw=3, label="imag")
            ax.grid()
            ax.legend()
            fs = 30
            ax.set_xlabel(r"$x$", fontsize=fs)
            ax.set_ylabel(r"$\psi_{%i}$" % n, fontsize=fs)
            canvas = FigureCanvas(fig)
            toolbar = NavigationToolbar(canvas, winPreviewBasis)
            vbox_PAGE1.append(toolbar)
            vbox_PAGE1.append(canvas, True)

            def updatePlot(btn, width):
                n = int(btn.get_value())
                yreal = eval(basis).real
                yimag = eval(basis).imag
                ax.clear()
                ax.plot(x, yreal, lw=3, label="real")
                ax.plot(x, yimag, lw=3, label="imag")
                ax.grid()
                ax.legend()
                ax.set_xlabel(r"$x$", fontsize=fs)
                ax.set_ylabel(r"$\psi_{%i}$" % n, fontsize=fs)
                vbox_PAGE1.reorder_child(canvas, 4)
            btn_n_PAGE1.connect("value-changed", updatePlot, width)
            winPreviewBasis.maximize()
            winPreviewBasis.show_all()
        except Exception as e:
            dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
                                       Gtk.ButtonsType.OK,
                                       "WARNING")
            dialog.format_secondary_text(str(e))
            dialog.run()

            dialog.destroy()

    btnPreviewBasis_PAGE1.connect("clicked", preview_basis, txtWidth,
                            txtDivisions, txtAmountBasis, txtBasis)

    hbox1_PAGE1.append(lblBasis_PAGE1)
    hbox1_PAGE1.append(txtBasis, True)
    hbox1_PAGE1.append(chkDefaultBasis)
    hbox1_PAGE1.append(btnPreviewBasis_PAGE1)

    lblPotential_PAGE1 = Label("Potential")
    txtPotential = Entry("Potential of the particle")
    cbbPotentials = Gtk.ComboBoxText()
    cbbPotentials.append_text("Other")
    cbbPotentials.append_text("Harmonic Oscillator")
    cbbPotentials.append_text("Inverted Harmonic Oscillator")
    cbbPotentials.append_text("Step")
    cbbPotentials.append_text("Barrier")
    cbbPotentials.append_text("Well")
    cbbPotentials.append_text("Linear Well")
    cbbPotentials.append_text("Inverted Linear Well")
    cbbPotentials.append_text("Abs Sinc")
    btnPreviewPotential_PAGE1 = Button("Preview")

    hbox2_PAGE1.append(lblPotential_PAGE1),
    hbox2_PAGE1.append(txtPotential, True)
    hbox2_PAGE1.append(cbbPotentials)
    hbox2_PAGE1.append(btnPreviewPotential_PAGE1)

    def updatePotential(cbb, potential):
        text = cbb.get_active_text()
        if text == "Harmonic Oscillator":
            potential.set_text("VALUE1**2 * 0.5 * (x - width/2)**2")
        elif text == "Inverted Harmonic Oscillator":
            potential.set_text("VALUE1**2 * 0.5 * (x * width - x**2)")
        elif text == "Step":
            potential.set_text("VALUE1 * heaviside(x - VALUE2)")
        elif text == "Barrier":
            potential.set_text("VALUE1 * heaviside(x - width / 2 + VALUE2) * (1 - heaviside(x - width / 2 - VALUE2))")
        elif text == "Well":
            potential.set_text("VALUE1 * (heaviside(width / 2 - VALUE2 - x) + heaviside(x - width / 2 - VALUE2))")
        elif text == "Linear Well":
            potential.set_text("VALUE1 * (heaviside(x - width / 2) * x + heaviside(width / 2 - x) * (1 - x))")
        elif text == "Inverted Linear Well":
            potential.set_text("VALUE1 * (heaviside(x - width / 2) * (1 - x) + heaviside(width / 2 - x) * x)")
        elif text == "Abs Sinc":
            potential.set_text("VALUE1 * abs(sinc(VALUE2 * (x - width / 2)))")
        elif text == "Other":
            potential.set_text("")

    cbbPotentials.connect("changed", updatePotential, txtPotential)

    def preview_potential(widget, txtWidth, txtDivisions, txtPotential):
        try:
            width = float(txtWidth.get_text())
            ndiv = int(txtDivisions.get_text())
            try:
                float(txtPotential.get_text())
                potential = "numpy.ones(ndiv) * float(txtPotential.get_text())"
            except ValueError:
                potential = txtPotential.get_text()
            vbox_PAGE1 = VBox()
            winPreviwePotential = Window("Preview of potential")
            winPreviwePotential.add(vbox_PAGE1)

            fig = pyplot.figure()
            ax = fig.add_subplot(111)
            x = numpy.linspace(0, width, ndiv)
            y = eval(potential)
            ax.plot(x, y, "-r", lw=3)
            fs = 30
            ax.set_xlabel(r"$x$", fontsize=fs)
            ax.set_ylabel(r"$V\left(x\right)$", fontsize=fs)
            ax.grid()
            canvas = FigureCanvas(fig)
            toolbar = NavigationToolbar(canvas, winPreviwePotential)
            vbox_PAGE1.append(toolbar)
            vbox_PAGE1.append(canvas, True)
            winPreviwePotential.maximize()
            winPreviwePotential.show_all()
        except Exception as e:
            dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
                                       Gtk.ButtonsType.OK,
                                       "WARNING")
            dialog.format_secondary_text(str(e))
            dialog.run()

            dialog.destroy()

    btnPreviewPotential_PAGE1.connect("clicked", preview_potential, txtWidth,
                                txtDivisions, txtPotential)

    figLevels = pyplot.figure()
    canvasLevels = FigureCanvas(figLevels)
    toolbar = NavigationToolbar(canvasLevels, window)

    def exportDataLevels(widget, txtWidth, txtDivisions,
                         txtAmountBasis, txtBasis, txtPotential):
        try:
            width = float(txtWidth.get_text())
            ndiv = int(txtDivisions.get_text())
            N = int(txtAmountBasis.get_text())
            basis = txtBasis.get_text()
            potential = txtPotential.get_text()
            dialogLevels = Gtk.FileChooserDialog(
                "Choose a file", window,
                Gtk.FileChooserAction.SAVE, (
                    Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
            fileFilter = Gtk.FileFilter()
            fileFilter.set_name("Text file")
            fileFilter.add_mime_type("text/plain")
            dialogLevels.add_filter(fileFilter)
            response = dialogLevels.run()
            if response == Gtk.ResponseType.OK:
                file_ = open(dialogLevels.get_filename(), mode="w")
                file_.write
                file_.write("Potential = V[x] = %s\n" % potential)
                file_.write("Basis = psi_n[x] = %s\n" % basis)
                file_.write("Amount of basis = %i\n" % N)
                file_.write("Width = %f\n" % width)
                file_.write("Amount of divisions = %i\n" % ndiv)
                file_.write("#n\tE_n\n")
                for i, val in enumerate(E_ordered):
                    file_.write("{}\t{}\n".format(i, E_ordered[i]))
                file_.close()
                dialogLevels.destroy()
            else:
                dialogLevels.destroy()
        except Exception as e:
            dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
                                       Gtk.ButtonsType.OK,
                                       "WARNING")
            dialog.format_secondary_text(str(e))
            dialog.run()
            dialog.destroy()
            dialogLevels.destroy()

    btnExporDataLevels_PAGE1 = Button("Export data")
    btnExporDataLevels_PAGE1.connect("clicked", exportDataLevels,
                               txtWidth, txtDivisions,
                               txtAmountBasis, txtBasis, txtPotential)
    hbox_save_levels_PAGE1 = HBox()
    hbox_save_levels_PAGE1.append(toolbar, True)
    hbox_save_levels_PAGE1.append(btnExporDataLevels_PAGE1)
    vbox1_PAGE1.append(hbox_save_levels_PAGE1)
    vbox1_PAGE1.append(canvasLevels, True)

    btnCompute_PAGE1 = Button("Compute")
    hbox3_PAGE1.append(btnCompute_PAGE1, False)

    def compute(widget, txtWidth, txtDivisions,
                txtAmountBasis, txtBasis, txtPotential):
        global E_ordered, C_ordered, C_prime
        widget.set_sensitive(False)
        hbox7_PAGE1.show()

        try:
            width = float(txtWidth.get_text())
            ndiv = int(txtDivisions.get_text())
            N = int(txtAmountBasis.get_text())
            basis = txtBasis.get_text()
            try:
                float(txtPotential.get_text())
                potential = "numpy.ones(ndiv) * float(txtPotential.get_text())"
            except ValueError:
                potential = txtPotential.get_text()

            x = numpy.linspace(0, width, ndiv)
            data_potential = eval(potential)

            def basis_n(n, width=width, basis=basis, x=x):
                return eval(basis)

            values_n = range(N)
            A = {}
            for m in values_n:
                A[m] = simps(numpy.conjugate(basis_n(m + 1)) * basis_n(
                    m + 1), x).real

            data_basis = {}
            for m in values_n:
                data_basis[m] = (basis_n(m + 1) / sqrt(A[m]))

            norms = {}
            for m in values_n:
                norms[m] = (simps(numpy.conjugate(
                    data_basis[m]) * data_basis[m], x)).real

            S, T, V = get_Matrices(values_n, data_basis, data_potential, x)
            H = T + V

            E, C, C_prime = eigen_vec_vals(S, values_n, ERROR, H)
            E_ordered = numpy.sort(E)

            E_ordered = numpy.zeros_like(E, dtype="float")
            C_ordered = numpy.zeros_like(C)

            for i, e in enumerate(numpy.argsort(E)):
                E_ordered[i] = E[e].real
                C_ordered[:, i] = C[:, e]

            psi_n_real = numpy.zeros(shape=(N, ndiv))
            psi_n_imag = numpy.zeros(shape=(N, ndiv), dtype="complex128")
            for n in range(N):
                for i in range(N):
                    psi_n_real[n] += (C_ordered[i, n] * data_basis[i]).real
                    psi_n_imag[n] += (C_ordered[i, n] * data_basis[i]).imag

            imgNormalized_PAGE1.set_from_file("ok.png")
            for val in A:
                if abs(A[val] - 1.0) >= ERROR:
                    imgNormalized_PAGE1.set_from_file("bad.png")
                    break
            imgNormalized_PAGE1.show()

            imgCanNormalized_PAGE1.set_from_file("ok.png")
            for val in norms:
                if abs(norms[val] - 1.0) >= ERROR:
                    imgCanNormalized_PAGE1.set_from_file("bad.png")
                    break
            imgCanNormalized_PAGE1.show()

            if numpy.allclose(S, numpy.identity(N), atol=ERROR):
                imgOrthonormal_PAGE1.set_from_file("ok.png")
            else:
                imgOrthonormal_PAGE1.set_from_file("bad.png")
            imgOrthonormal_PAGE1.show()

            a = figLevels.add_subplot(111)
            a.clear()
            a.plot(x, data_potential, "-g", lw=5)
            for i, ene in enumerate(E_ordered):
                a.text(width/2, ene, r"$E_{%i} = %f$" % (
                    i+1, ene.real), fontsize=14, va="bottom", ha="center")
                a.plot([0, width], [ene, ene], lw=2, c="k")

            upper = E_ordered[-1] + (E_ordered[-1] - E_ordered[-2])
            a.plot([0, 0], [data_potential[0], max([upper, max(data_potential)])], lw=5, c="g")
            a.plot([width, width], [data_potential[-1], max([upper, max(data_potential)])], lw=5, c="g")
            a.grid()
            a.set_xlim(-0.1, width+0.1)
            a.set_ylim(ymax=max([upper, max(data_potential)]))
            a.set_ylabel(r"$V\left(x\right)$", fontsize=30, color="green")
            a.set_xlabel(r"$x$", fontsize=30)
            vbox1_PAGE1.reorder_child(canvasLevels, 5)

            if widget.get_label() == "Probability density function":
                try:
                    vbox_PAGE1 = VBox()
                    hbox_PAGE1 = HBox()
                    vbox_PAGE1.append(hbox_PAGE1)
                    winProbabilityFunction = Window(
                        "Probability density function")
                    winProbabilityFunction.add(vbox_PAGE1)

                    lbl_n_PAGE1 = Label("n = ")
                    lbl_n_PAGE1.set_tooltip_text("Choose the number of basis 'n'")
                    hbox_PAGE1.append(lbl_n_PAGE1)
                    btn_n_PAGE1 = Gtk.SpinButton.new_with_range(1, N, 1)
                    hbox_PAGE1.append(btn_n_PAGE1, True)

                    n = int(btn_n_PAGE1.get_value())

                    fs = 30
                    fig = pyplot.figure()
                    ax = fig.add_subplot(111)
                    x = numpy.linspace(0, width, ndiv)
                    yreal = numpy.conjugate(psi_n_real[n-1]) * psi_n_real[n-1]
                    yimag = numpy.conjugate(psi_n_imag[n-1]) * psi_n_imag[n-1]
                    y = yreal.real + yimag.real
                    P = simps(y, x).real
                    ax.plot(x, y, "-g", lw=3)
                    ax.grid()
                    ax.set_title(r"$E_{%i}=%0.4f$" % (n, E_ordered[n-1]), fontsize=fs)
                    ax.set_xlabel(r"$x$", fontsize=fs)
                    ax.set_ylabel(r"$\left|\psi_{%i}\right|^2$" % n,
                                  fontsize=fs)

                    ax_potential = ax.twinx()
                    ax_potential.plot(x, data_potential, "-r", lw=5)
                    ax_potential.plot([0, width],
                                      [E_ordered[n-1], E_ordered[n-1]],
                                      "-k", lw=5)
                    ax_potential.set_ylabel(r"$V\left(x\right)$", color="r",
                                            fontsize=fs)
                    canvas = FigureCanvas(fig)

                    btnExporData_PAGE1 = Button("Export data")
                    toolbar = NavigationToolbar(canvas, winProbabilityFunction)
                    hbox_save_PAGE1 = HBox()
                    hbox_save_PAGE1.append(toolbar, True)
                    hbox_save_PAGE1.append(btnExporData_PAGE1)
                    vbox_PAGE1.append(hbox_save_PAGE1)
                    vbox_PAGE1.append(canvas, True)

                    def updatePlot(cbb_n, width=width):
                        n = int(cbb_n.get_value())
                        yreal = numpy.conjugate(psi_n_real[n-1]) * psi_n_real[n-1]
                        yimag = numpy.conjugate(psi_n_imag[n-1]) * psi_n_imag[n-1]
                        y = yreal.real + yimag.real
                        P = simps(y, x).real
                        
                        ax.clear()
                        ax_potential.clear()
                        ax.plot(x, y, "-g", lw=3)
                        ax.grid()
                        ax.set_title(r"$E_{%i}=%0.4f$" % (n, E_ordered[n-1]), fontsize=fs)
                        ax.set_xlabel(r"$x$", fontsize=fs)
                        ax.set_ylabel(r"$\left|\psi_{%i}\right|^2$" % n,
                                      fontsize=fs)

                        ax_potential.plot(x, data_potential, "-r", lw=5)
                        ax_potential.plot([0, width],
                                          [E_ordered[n-1],
                                           E_ordered[n-1]],
                                          "-k", lw=5)
                        ax_potential.set_ylabel(r"$V\left(x\right)$",
                                                color="r",
                                                fontsize=fs)

                        vbox_PAGE1.reorder_child(canvas, 4)
                    btn_n_PAGE1.connect("value-changed", updatePlot)

                    def exportData(widget, cbb_n, width=width):
                        n = int(cbb_n.get_value())
                        yreal = numpy.conjugate(psi_n_real[n-1]) * psi_n_real[n-1]
                        yimag = numpy.conjugate(psi_n_imag[n-1]) * psi_n_imag[n-1]
                        y = yreal.real + yimag.real
                        P = simps(y, x).real
                        dialogProbabilityFunction = Gtk.FileChooserDialog(
                            "Choose a file", winProbabilityFunction,
                            Gtk.FileChooserAction.SAVE, (
                                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
                        fileFilter = Gtk.FileFilter()
                        fileFilter.set_name("Text file")
                        fileFilter.add_mime_type("text/plain")
                        dialogProbabilityFunction.add_filter(fileFilter)
                        response = dialogProbabilityFunction.run()
                        if response == Gtk.ResponseType.OK:
                            file_ = open(
                                dialogProbabilityFunction.get_filename(),
                                mode="w")
                            file_.write("#E_%i=%f\tP=%f\n" % (
                                n, E_ordered[n-1], P))
                            file_.write("#x\t|psi_%i|^2\tV[x]\n" % n)
                            for i, val in enumerate(x):
                                file_.write("{}\t{}\t{}\n".format(
                                    x[i], y[i], data_potential[i]))
                            file_.close()
                            dialogProbabilityFunction.destroy()
                        else:
                            dialogProbabilityFunction.destroy()

                    btnExporData_PAGE1.connect("clicked", exportData, btn_n_PAGE1)
                    winProbabilityFunction.maximize()
                    winProbabilityFunction.show_all()
                except Exception as e:
                    dialog = Gtk.MessageDialog(
                        window, 0, Gtk.MessageType.WARNING,
                        Gtk.ButtonsType.OK,
                        "WARNING")
                    dialog.format_secondary_text(str(e))
                    dialog.run()

                    dialog.destroy()
                    dialogProbabilityFunction.destroy()

            if widget.get_label() == "Wavefunction":
                try:
                    vbox_PAGE1 = VBox()
                    hbox_PAGE1 = HBox()
                    vbox_PAGE1.append(hbox_PAGE1)
                    winWaveFunction = Window("Wavefunction")
                    winWaveFunction.add(vbox_PAGE1)

                    lbl_n_PAGE1 = Label("n = ")
                    lbl_n_PAGE1.set_tooltip_text("Choose the number of basis 'n'")
                    hbox_PAGE1.append(lbl_n_PAGE1)
                    btn_n_PAGE1 = Gtk.SpinButton.new_with_range(1, N, 1)
                    hbox_PAGE1.append(btn_n_PAGE1, True)

                    n = int(btn_n_PAGE1.get_value())

                    fs = 30
                    fig = pyplot.figure()
                    ax = fig.add_subplot(111)
                    x = numpy.linspace(0, width, ndiv)
                    yreal = psi_n_real[n-1]
                    yimag = psi_n_imag[n-1]
                    ax.plot(x, yreal, "-g", lw=3, label="Real")
                    ax.plot(x, yimag, "-b", lw=3, label="Imag")
                    ax.grid()
                    ax.set_title(r"$E_{%i}=%0.4f$" % (n, E_ordered[n-1]), fontsize=fs)
                    ax.set_xlabel(r"$x$", fontsize=fs)
                    ax.set_ylabel(r"$\psi_{%i}$" % n, fontsize=fs)
                    ax.legend()

                    ax_potential = ax.twinx()
                    ax_potential.plot(x, data_potential, "-r", lw=5)
                    ax_potential.plot([0, width],
                                      [E_ordered[n-1], E_ordered[n-1]],
                                      "-k", lw=5)
                    ax_potential.set_ylabel(r"$V\left(x\right)$", color="r",
                                            fontsize=fs)
                    canvas = FigureCanvas(fig)

                    btnExporData_PAGE1 = Button("Export data")
                    toolbar = NavigationToolbar(canvas, winWaveFunction)
                    hbox_save_PAGE1 = HBox()
                    hbox_save_PAGE1.append(toolbar, True)
                    hbox_save_PAGE1.append(btnExporData_PAGE1)
                    vbox_PAGE1.append(hbox_save_PAGE1)
                    vbox_PAGE1.append(canvas, True)

                    def updatePlot(cbb_n, width=width):
                        n = int(cbb_n.get_value())
                        ax.clear()
                        ax_potential.clear()
                        yreal = psi_n_real[n-1]
                        yimag = psi_n_imag[n-1]
                        ax.plot(x, yreal, "-g", lw=3, label="Real")
                        ax.plot(x, yimag, "-b", lw=3, label="Imag")
                        ax.grid()
                        ax.set_title(r"$E_{%i}=%0.4f$" % (n, E_ordered[n-1]), fontsize=fs)
                        ax.set_xlabel(r"$x$", fontsize=fs)
                        ax.set_ylabel(r"$\psi_{%i}$" % n, fontsize=fs)
                        ax.legend()

                        ax_potential.plot(x, data_potential, "-r", lw=5)
                        ax_potential.plot([0, width],
                                          [E_ordered[n-1], E_ordered[n-1]],
                                          "-k", lw=5)
                        ax_potential.set_ylabel(r"$V\left(x\right)$",
                                                color="r",
                                                fontsize=fs)

                        vbox_PAGE1.reorder_child(canvas, 4)
                    btn_n_PAGE1.connect("value-changed", updatePlot)

                    def exportData(widget, cbb_n, width=width):
                        n = int(cbb_n.get_value())
                        yreal = psi_n_real[n-1]
                        yimag = psi_n_imag[n-1]
                        dialogWaveFunction = Gtk.FileChooserDialog(
                            "Choose a file", winWaveFunction,
                            Gtk.FileChooserAction.SAVE, (
                                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
                        fileFilter = Gtk.FileFilter()
                        fileFilter.set_name("Text file")
                        fileFilter.add_mime_type("text/plain")
                        dialogWaveFunction.add_filter(fileFilter)
                        response = dialogWaveFunction.run()
                        if response == Gtk.ResponseType.OK:
                            file_ = open(dialogWaveFunction.get_filename(),
                                         mode="w")
                            file_.write("#E_%i=%f\n" % (
                                n, E_ordered[n-1]))
                            file_.write("#x\t|psi_%i|^2\tV[x]\n" % n)
                            for i, val in enumerate(x):
                                file_.write("{}\t{}\t{}\t{}\n".format(
                                    x[i], yreal[i], yimag[i], data_potential[i]))
                            file_.close()
                            dialogWaveFunction.destroy()
                        else:
                            dialogWaveFunction.destroy()

                    btnExporData_PAGE1.connect("clicked", exportData, btn_n_PAGE1)
                    winWaveFunction.maximize()
                    winWaveFunction.show_all()
                except Exception as e:
                    dialog = Gtk.MessageDialog(
                        window, 0, Gtk.MessageType.WARNING,
                        Gtk.ButtonsType.OK,
                        "WARNING")
                    dialog.format_secondary_text(str(e))
                    dialog.run()

                    dialog.destroy()
        except Exception as e:
            dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
                                       Gtk.ButtonsType.OK,
                                       "WARNING")
            dialog.format_secondary_text(str(e))
            dialog.run()

            dialog.destroy()
        widget.set_sensitive(True)

    btnCompute_PAGE1.connect("clicked", compute, txtWidth, txtDivisions,
                       txtAmountBasis, txtBasis, txtPotential)
    btnProbabilityFunction_PAGE1.connect("clicked", compute, txtWidth, txtDivisions,
                                   txtAmountBasis, txtBasis, txtPotential)
    btnWaveFunction_PAGE1.connect("clicked", compute, txtWidth, txtDivisions,
                            txtAmountBasis, txtBasis, txtPotential)















    hbox0_PAGE2 = HBox()
    hbox1_PAGE2 = HBox()
    hbox2_PAGE2 = HBox()
    hbox3_PAGE2 = HBox()
    hbox7_PAGE2 = HBox()
    vbox0_PAGE2 = VBox()
    vbox1_PAGE2 = VBox()

    parentPeriodicPotential_PAGE2.append(hbox0_PAGE2)
    parentPeriodicPotential_PAGE2.append(hbox1_PAGE2)
    parentPeriodicPotential_PAGE2.append(hbox2_PAGE2)
    parentPeriodicPotential_PAGE2.append(hbox3_PAGE2)
    parentPeriodicPotential_PAGE2.append(hbox7_PAGE2, True)

    hbox7_PAGE2.append(vbox0_PAGE2)
    hbox7_PAGE2.append(vbox1_PAGE2, True)

    lblNormalized_PAGE2 = Label("Basis set is normalized ?")
    imgNormalized_PAGE2 = Image()
    hbox4_PAGE2 = HBox()
    hbox4_PAGE2.append(lblNormalized_PAGE2)
    hbox4_PAGE2.append(imgNormalized_PAGE2)

    lblCanNormalized_PAGE2 = Label("Basis set can be normalized ?")
    imgCanNormalized_PAGE2 = Image()
    hbox5_PAGE2 = HBox()
    hbox5_PAGE2.append(lblCanNormalized_PAGE2)
    hbox5_PAGE2.append(imgCanNormalized_PAGE2)

    lblOrthonormal_PAGE2 = Label("Basis set is orthonormal ?")
    imgOrthonormal_PAGE2 = Image()
    hbox6_PAGE2 = HBox()
    hbox6_PAGE2.append(lblOrthonormal_PAGE2)
    hbox6_PAGE2.append(imgOrthonormal_PAGE2)

    lblPeriodic_PAGE2 = Label("Basis set is periodic ?")
    imgPeriodic_PAGE2 = Image()
    hboxPeriodic_PAGE2 = HBox()
    hboxPeriodic_PAGE2.append(lblPeriodic_PAGE2)
    hboxPeriodic_PAGE2.append(imgPeriodic_PAGE2)

    lblWidth_PAGE2 = Label("Width      ")
    hbox0_PAGE2.append(lblWidth_PAGE2)

    txtWidth_PAGE2 = Entry("With of the box")
    txtWidth_PAGE2.set_text("1.0")
    hbox0_PAGE2.append(txtWidth_PAGE2)

    lblDivsions_PAGE2 = Label("Divisions")
    hbox0_PAGE2.append(lblDivsions_PAGE2)

    txtDivisions_PAGE2 = Entry("The amount of intervals in the space")
    txtDivisions_PAGE2.set_text("100")
    hbox0_PAGE2.append(txtDivisions_PAGE2)

    lblAmountBasis_PAGE2 = Label("Amount of basis")
    hbox0_PAGE2.append(lblAmountBasis_PAGE2)

    txtAmountBasis_PAGE2 = Entry("Insert amount of the basis")
    txtAmountBasis_PAGE2.set_text("10")
    hbox0_PAGE2.append(txtAmountBasis_PAGE2)

    lblNdivK_PAGE2 = Label("Amount of points for K")
    hbox0_PAGE2.append(lblNdivK_PAGE2)

    txtNdivK_PAGE2 = Entry("Insert amount of points for K")
    txtNdivK_PAGE2.set_text("50")
    hbox0_PAGE2.append(txtNdivK_PAGE2)

    lblBasis_PAGE2 = Label("Basis set ")
    txtBasis_PAGE2 = Entry("Set of basis")
    chkDefaultBasis = Gtk.CheckButton("Default")
    chkDefaultBasis.connect("toggled", lambda chk: txtBasis_PAGE2.set_text(
        "sqrt(1 / width) * exp(1.0j * (K + 2 * pi * n / width) * x)")
        if chk.get_active()
        else txtBasis_PAGE2.set_text(""))
    btnPreviewBasis_PAGE2 = Button("Preview")

    def preview_basis_PAGE2(widget, txtWidth_PAGE2, txtDivisions_PAGE2,
                            txtAmountBasis_PAGE2, txtBasis_PAGE2, txtNdivK_PAGE2):
        try:
            global btn_n_PAGE2
            global btn_K_PAGE2
            width = float(txtWidth_PAGE2.get_text())
            ndiv = int(txtDivisions_PAGE2.get_text())
            ndiv_k = int(txtNdivK_PAGE2.get_text())
            N = int(txtAmountBasis_PAGE2.get_text())
            basis = txtBasis_PAGE2.get_text()

            vbox_PAGE2 = VBox()
            hbox_PAGE2 = HBox()
            hbox_K_PAGE2 = HBox()
            vbox_PAGE2.append(hbox_PAGE2)
            vbox_PAGE2.append(hbox_K_PAGE2)
            winPreviewBasis = Window("Preview of basis set")
            winPreviewBasis.add(vbox_PAGE2)

            lbl_n_PAGE2 = Label("n = ")
            lbl_n_PAGE2.set_tooltip_text("Choose the number of basis 'n'")
            hbox_PAGE2.append(lbl_n_PAGE2)
            btn_n_PAGE2 = Gtk.SpinButton.new_with_range(-N, N, 1)
            hbox_PAGE2.append(btn_n_PAGE2, True)

            lbl_K_PAGE2 = Label("K = ")
            lbl_K_PAGE2.set_tooltip_text("Choose the value for 'K'")
            hbox_K_PAGE2.append(lbl_K_PAGE2)
            btn_K_PAGE2 = Gtk.SpinButton.new_with_range(-pi/width, pi/width, 0.001)
            hbox_K_PAGE2.append(btn_K_PAGE2, True)

            n = int(btn_n_PAGE2.get_value())
            K = float(btn_K_PAGE2.get_value())

            fig = pyplot.figure()
            ax = fig.add_subplot(111)
            x = numpy.linspace(0, width, ndiv)
            yreal = eval(basis).real
            yimag = eval(basis).imag
            ax.plot(x, yreal, lw=3, label="real")
            ax.plot(x, yimag, lw=3, label="imag")
            ax.grid()
            ax.legend()
            fs = 30
            ax.set_xlabel(r"$x$", fontsize=fs)
            ax.set_ylabel(r"$\psi_{%i}$" % n, fontsize=fs)
            # fig.tight_layout()
            canvas = FigureCanvas(fig)
            toolbar_page2 = NavigationToolbar(canvas, winPreviewBasis)
            vbox_PAGE2.append(toolbar_page2)
            vbox_PAGE2.append(canvas, True)

            def updatePlot(some, width):
                global btn_n_PAGE2
                global btn_K_PAGE2
                n = int(btn_n_PAGE2.get_value())
                K = float(btn_K_PAGE2.get_value())
                yreal = eval(basis).real
                yimag = eval(basis).imag
                ax.clear()
                ax.plot(x, yreal, lw=3, label="real")
                ax.plot(x, yimag, lw=3, label="imag")
                ax.grid()
                ax.legend()
                ax.set_xlabel(r"$x$", fontsize=fs)
                ax.set_ylabel(r"$\psi_{%i}$" % n, fontsize=fs)
                vbox_PAGE2.reorder_child(canvas, 4)

            btn_n_PAGE2.connect("value-changed", updatePlot, width)
            btn_K_PAGE2.connect("value-changed", updatePlot, width)
            winPreviewBasis.maximize()
            winPreviewBasis.show_all()
        except Exception as e:
            dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
                                       Gtk.ButtonsType.OK,
                                       "WARNING")
            dialog.format_secondary_text(str(e))
            dialog.run()

            dialog.destroy()

    btnPreviewBasis_PAGE2.connect("clicked", preview_basis_PAGE2, txtWidth_PAGE2,
                            txtDivisions_PAGE2, txtAmountBasis_PAGE2, txtBasis_PAGE2, txtNdivK_PAGE2)

    hbox1_PAGE2.append(lblBasis_PAGE2)
    hbox1_PAGE2.append(txtBasis_PAGE2, True)
    hbox1_PAGE2.append(chkDefaultBasis)
    hbox1_PAGE2.append(btnPreviewBasis_PAGE2)

    lblPotential_PAGE2 = Label("Potential")
    txtPotential_PAGE2 = Entry("Potential of the particle")
    cbbPotentials = Gtk.ComboBoxText()
    cbbPotentials.append_text("Other")
    cbbPotentials.append_text("Harmonic Oscillator")
    cbbPotentials.append_text("Inverted Harmonic Oscillator")
    cbbPotentials.append_text("Step")
    cbbPotentials.append_text("Barrier")
    cbbPotentials.append_text("Well")
    cbbPotentials.append_text("Linear Well")
    cbbPotentials.append_text("Inverted Linear Well")
    cbbPotentials.append_text("Abs Sinc")
    btnPreviewPotential_PAGE2 = Button("Preview")

    hbox2_PAGE2.append(lblPotential_PAGE2),
    hbox2_PAGE2.append(txtPotential_PAGE2, True)
    hbox2_PAGE2.append(cbbPotentials)
    hbox2_PAGE2.append(btnPreviewPotential_PAGE2)

    def updatePotential_PAGE2(cbb, potential):
        text = cbb.get_active_text()
        if text == "Harmonic Oscillator":
            potential.set_text("VALUE1**2 * 0.5 * (x - width/2)**2")
        elif text == "Inverted Harmonic Oscillator":
            potential.set_text("VALUE1**2 * 0.5 * (x * width - x**2)")
        elif text == "Step":
            potential.set_text("VALUE1 * heaviside(x - VALUE2)")
        elif text == "Barrier":
            potential.set_text("VALUE1 * heaviside(x - width / 2 + VALUE2) * (1 - heaviside(x - width / 2 - VALUE2))")
        elif text == "Well":
            potential.set_text("VALUE1 * (heaviside(width / 2 - VALUE2 - x) + heaviside(x - width / 2 - VALUE2))")
        elif text == "Linear Well":
            potential.set_text("VALUE1 * (heaviside(x - width / 2) * x + heaviside(width / 2 - x) * (1 - x))")
        elif text == "Inverted Linear Well":
            potential.set_text("VALUE1 * (heaviside(x - width / 2) * (1 - x) + heaviside(width / 2 - x) * x)")
        elif text == "Abs Sinc":
            potential.set_text("VALUE1 * abs(sinc(VALUE2 * (x - width / 2)))")
        elif text == "Other":
            potential.set_text("")

    cbbPotentials.connect("changed", updatePotential_PAGE2, txtPotential_PAGE2)

    def preview_potential_PAGE2(widget, txtWidth_PAGE2, txtDivisions_PAGE2, txtPotential_PAGE2):
        try:
            width = float(txtWidth_PAGE2.get_text())
            ndiv = int(txtDivisions_PAGE2.get_text())
            try:
                float(txtPotential_PAGE2.get_text())
                potential = "numpy.ones(ndiv) * float(txtPotential_PAGE2.get_text())"
            except ValueError:
                potential = txtPotential_PAGE2.get_text()
            vbox_PAGE2 = VBox()
            winPreviwePotential = Window("Preview of potential")
            winPreviwePotential.add(vbox_PAGE2)

            fig = pyplot.figure()
            ax = fig.add_subplot(111)
            x = numpy.linspace(0, width, ndiv)
            y = eval(potential)
            ax.plot(x, y, "-r", lw=3)
            fs = 30
            ax.set_xlabel(r"$x$", fontsize=fs)
            ax.set_ylabel(r"$V\left(x\right)$", fontsize=fs)
            ax.grid()
            # fig.tight_layout()
            canvas = FigureCanvas(fig)
            toolbar_page2 = NavigationToolbar(canvas, winPreviwePotential)
            vbox_PAGE2.append(toolbar_page2)
            vbox_PAGE2.append(canvas, True)
            winPreviwePotential.maximize()
            winPreviwePotential.show_all()
        except Exception as e:
            dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
                                       Gtk.ButtonsType.OK,
                                       "WARNING")
            dialog.format_secondary_text(str(e))
            dialog.run()

            dialog.destroy()

    btnPreviewPotential_PAGE2.connect("clicked", preview_potential_PAGE2, txtWidth_PAGE2,
                                txtDivisions_PAGE2, txtPotential_PAGE2)

    fibBands_PAGE2 = pyplot.figure()
    # fibBands_PAGE2.tight_layout()
    canvasLevels_PAGE2 = FigureCanvas(fibBands_PAGE2)
    toolbar_page2 = NavigationToolbar(canvasLevels_PAGE2, window)

    def export_band_structure(widget, txtWidth_PAGE2, txtDivisions_PAGE2,
                         txtAmountBasis_PAGE2, txtBasis_PAGE2, txtPotential_PAGE2):
        try:
            global energies, K_arr
            width = float(txtWidth_PAGE2.get_text())
            ndiv = int(txtDivisions_PAGE2.get_text())
            ndiv_k = int(txtNdivK_PAGE2.get_text())
            N = int(txtAmountBasis_PAGE2.get_text())
            basis = txtBasis_PAGE2.get_text()
            potential = txtPotential_PAGE2.get_text()
            dialogLevels = Gtk.FileChooserDialog(
                "Choose a file", window,
                Gtk.FileChooserAction.SAVE, (
                    Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
            fileFilter = Gtk.FileFilter()
            fileFilter.set_name("Text file")
            fileFilter.add_mime_type("text/plain")
            dialogLevels.add_filter(fileFilter)
            response = dialogLevels.run()
            if response == Gtk.ResponseType.OK:
                file_ = open(dialogLevels.get_filename(), mode="w")
                
                file_.write("Potential = V[x] = %s\n" % potential)
                file_.write("Basis = psi_n[x] = %s\n" % basis)
                file_.write("Amount of basis = %i\n" % N)
                file_.write("Width = %f\n" % width)
                file_.write("Amount of divisions for x = %i\n" % ndiv)
                file_.write("Amount of divisions for K = %i\n" % ndiv_k)
                file_.write("#K*width/pi\t")
                for n, val in enumerate(energies):
                    file_.write("E_%i\t" % (n+1))
                file_.write("\n")
                for k, Ka in enumerate(K_arr):
                    file_.write("%f\t" % (Ka * width / pi))
                    for i, ene in enumerate(energies[k, :]):
                        file_.write("%f\t" % ene)
                    file_.write("\n")

                file_.close()
                dialogLevels.destroy()
            else:
                dialogLevels.destroy()
        except Exception as e:
            dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
                                       Gtk.ButtonsType.OK,
                                       "WARNING")
            dialog.format_secondary_text(str(e))
            dialog.run()
            dialog.destroy()
            dialogLevels.destroy()

    lbl_num_bands_PAGE2 = Label("n max")
    btn_num_bands_PAGE2 = Gtk.SpinButton.new_with_range(
        1, int(txtAmountBasis_PAGE2.get_text()), 1)
    btn_num_bands_PAGE2.set_value(int(txtAmountBasis_PAGE2.get_text()))


    btnExporDataLevels_PAGE2 = Button("Export data")
    btnExporDataLevels_PAGE2.connect("clicked", export_band_structure,
                               txtWidth_PAGE2, txtDivisions_PAGE2,
                               txtAmountBasis_PAGE2, txtBasis_PAGE2, txtPotential_PAGE2)

    hbox_save_levels_PAGE2 = HBox()
    hbox_save_levels_PAGE2.append(toolbar_page2, True)
    hbox_save_levels_PAGE2.append(lbl_num_bands_PAGE2)
    hbox_save_levels_PAGE2.append(btn_num_bands_PAGE2)
    hbox_save_levels_PAGE2.append(btnExporDataLevels_PAGE2)
    vbox1_PAGE2.append(hbox_save_levels_PAGE2)
    vbox1_PAGE2.append(canvasLevels_PAGE2, True)

    btnCompute_PAGE2 = Button("Compute")
    hbox3_PAGE2.append(btnCompute_PAGE2, False)

    def compute_k(widget, txtWidth_PAGE2, txtDivisions_PAGE2,
                  txtAmountBasis_PAGE2, txtBasis_PAGE2, txtPotential_PAGE2, txtNdivK_PAGE2):
        global E_ordered, C_ordered, C_prime, energies, K_arr
        widget.set_sensitive(False)
        btn_num_bands_PAGE2.set_value(int(txtAmountBasis_PAGE2.get_text()))
        hbox7_PAGE2.show()

        try:
            width = float(txtWidth_PAGE2.get_text())
            ndiv = int(txtDivisions_PAGE2.get_text())
            ndiv_k = int(txtNdivK_PAGE2.get_text())
            N = int(txtAmountBasis_PAGE2.get_text())
            basis = txtBasis_PAGE2.get_text()
            try:
                float(txtPotential_PAGE2.get_text())
                potential = "numpy.ones(ndiv) * float(txtPotential_PAGE2.get_text())"
            except ValueError:
                potential = txtPotential_PAGE2.get_text()

            x = numpy.linspace(0, width, ndiv)
            data_potential = eval(potential)

            def basis_n_K(n, K, width=width, basis=basis, x=x):
                return eval(basis)

            values_n = [0]
            for n in range(1, N+1):
                values_n.append(n)
                values_n.append(-n)

            K_arr = numpy.linspace(-pi/width, pi/width, ndiv_k)

            A = {}
            for m in values_n:
                A[m] = simps(numpy.conjugate(basis_n_K(m, 0)) * basis_n_K(
                    m, 0), x).real

            T = numpy.zeros(shape=(len(values_n), len(values_n), len(K_arr)))
            V = numpy.zeros(shape=(len(values_n), len(values_n), len(K_arr)))
            for k, K in enumerate(K_arr):
                data_basis = {}
                for m in values_n:
                    data_basis[m] = basis_n_K(m, K) / sqrt(A[m])
                    
                S_, T_, V_ = get_Matrices(values_n, data_basis, data_potential, x)
                T[:, :, k] = T_
                V[:, :, k] = V_
            H = T + V

            energies = list()
            for k, K in enumerate(K_arr):
                eigenenergies, _ = numpy.linalg.eig(H[:, :, k])
                energies.append(sorted(eigenenergies))
            energies = numpy.array(energies)

            fs = 30
            fibBands_PAGE2.clf()
            ax = fibBands_PAGE2.add_subplot(111)
            
            for n in range(N):
                ax.plot(K_arr * width / pi, energies[:, n], lw=2)

                if n==0:
                    min1 = max(energies[:, n])
                else:
                    max1 = min(energies[:, n])
                    ax.add_patch(Rectangle((min(K_arr) * width / numpy.pi, min1),
                                                       2 * max(K_arr) * width / numpy.pi, max1-min1,
                                                       facecolor="green",
                                                       alpha=0.2, lw=0))
                min1 = max(energies[:, n])

            ax.set_xlabel(r"$K \cdot width/\pi$", fontsize=fs)
            ax.set_ylabel(r"$E$", fontsize=fs)
            ax_potential = ax.twinx()
            lim_min = min([ax.get_ylim()[0], ax_potential.get_ylim()[0]])
            lim_max = max([ax.get_ylim()[1], ax_potential.get_ylim()[1]])
            ax_potential.set_ylim(lim_min, lim_max)
            ax.set_ylim(lim_min, lim_max)

            newx = numpy.sort(numpy.concatenate((x, -x)))
            hatch = "/"
            alpha=0.3
            fcol = "red"
            ax_potential.fill_between(x, 0, data_potential, hatch=hatch, facecolor=fcol, alpha=alpha)
            ax_potential.fill_between(-x, 0, data_potential, hatch=hatch, facecolor=fcol, alpha=alpha)

            ax_potential.set_ylabel(r"$V\left(x\right)$",
                                    color="r",
                                    fontsize=fs)
            fibBands_PAGE2.tight_layout()

            vbox1_PAGE2.reorder_child(canvasLevels_PAGE2, 2)

        except Exception as e:
            dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
                                       Gtk.ButtonsType.OK,
                                       "WARNING")
            dialog.format_secondary_text(str(e))
            dialog.run()

            dialog.destroy()
        widget.set_sensitive(True)



    btnCompute_PAGE2.connect("clicked", compute_k, txtWidth_PAGE2, txtDivisions_PAGE2,
                   txtAmountBasis_PAGE2, txtBasis_PAGE2, txtPotential_PAGE2, txtNdivK_PAGE2)


    def replot(widget):
        global K_arr, energies
        try:
            width = float(txtWidth_PAGE2.get_text())
            ndiv = int(txtDivisions_PAGE2.get_text())
            try:
                float(txtPotential_PAGE2.get_text())
                potential = "numpy.ones(ndiv) * float(txtPotential_PAGE2.get_text())"
            except ValueError:
                potential = txtPotential_PAGE2.get_text()

            x = numpy.linspace(0, width, ndiv)
            data_potential = eval(potential)
            nmax = int(widget.get_value())
            fs = 30
            fibBands_PAGE2.clf()
            ax = fibBands_PAGE2.add_subplot(111)
            
            for n in range(nmax):
                ax.plot(K_arr * width / pi, energies[:, n], lw=2)

                if n==0:
                    min1 = max(energies[:, n])
                else:
                    max1 = min(energies[:, n])
                    ax.add_patch(Rectangle((min(K_arr) * width / numpy.pi, min1),
                                                       2 * max(K_arr) * width / numpy.pi, max1-min1,
                                                       facecolor="green",
                                                       alpha=0.2, lw=0))
                min1 = max(energies[:, n])

            ax.set_xlabel(r"$K \cdot width/\pi$", fontsize=fs)
            ax.set_ylabel(r"$E$", fontsize=fs)
            ax_potential = ax.twinx()
            lim_min = min([ax.get_ylim()[0], ax_potential.get_ylim()[0]])
            lim_max = max([ax.get_ylim()[1], ax_potential.get_ylim()[1]])
            ax_potential.set_ylim(lim_min, lim_max)
            ax.set_ylim(lim_min, lim_max)

            newx = numpy.sort(numpy.concatenate((x, -x)))
            hatch = "/"
            alpha=0.3
            fcol = "red"
            ax_potential.fill_between(x, 0, data_potential, hatch=hatch, facecolor=fcol, alpha=alpha)
            ax_potential.fill_between(-x, 0, data_potential, hatch=hatch, facecolor=fcol, alpha=alpha)

            ax_potential.set_ylabel(r"$V\left(x\right)$",
                                    color="r",
                                    fontsize=fs)

            fibBands_PAGE2.tight_layout()

            vbox1_PAGE2.reorder_child(canvasLevels_PAGE2, 2)
        except Exception as e:
            dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
                                       Gtk.ButtonsType.OK,
                                       "WARNING")
            dialog.format_secondary_text(str(e))
            dialog.run()

            dialog.destroy()

    btn_num_bands_PAGE2.connect("value-changed", replot)




    window.maximize()
    notebook.append_page(parentUniquePotential__PAGE1, Gtk.Label('Unique potential'))
    notebook.append_page(parentPeriodicPotential_PAGE2, Gtk.Label('Periodic potential'))
    window.add(notebook)
    window.connect("delete-event", Gtk.main_quit)
    window.show_all()
    hbox7_PAGE1.hide()
    hbox7_PAGE2.hide()
    

    Gtk.main()



if __name__ == '__main__':
    main()
